MicroTik / RouterBoard template for PRTG.
===========================================

This project contains all the files necessary to create sensors for MicroTik / RouterBoard router from a template.

Download Instructions
=========================
 [A zip file containing all the files in the installation package can be downloaded directly using this link](https://gitlab.com/PRTG/Device-Templates/MicroTik/-/jobs/artifacts/master/download?job=PRTGDistZip).

Installation Instructions
=========================
Please see: [Generic PRTG custom sensor instructions INSTALL_PRTG.md](./INSTALL_PRTG.md)

Sensors
====
![MicroTik Sensors RB750Gr3](./Images/MicroTik-MultiIF.png)
MicroTik Sensors RB750Gr3

Status sensors
====
![MicroTik Status RB750Gr3](./Images/MicroTik-Status1.png)
Status (RB750Gr3)

![MicroTik Status 2](./Images/MicroTik-Status2.png)
Status

Other Sensors
====
![Extended Interface Info](./Images/MicroTik-IF-Ext.png)
Extended Interface Info

![Simple Queue Status Sensor](./Images/MicroTik-QueueStat.png)
Simple Queue Status Sensor

